package com.latihan.newsapp.main

import com.latihan.newsapp.model.NewsData

interface MainView {

    fun showNews(data: List<NewsData>)
    fun onError(throwable: Throwable)
    fun showLoading()
    fun hideLoading()
}