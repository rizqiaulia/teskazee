package com.latihan.newsapp.main

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.latihan.newsapp.R
import com.latihan.newsapp.adapter.NewsRVAdapter
import com.latihan.newsapp.detail.DetailActivity
import com.latihan.newsapp.model.NewsData
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast




class MainActivity : AppCompatActivity(), MainView {


    private lateinit var presenter: MainPresenter
    private var listNews: MutableList<NewsData> = mutableListOf()
    private lateinit var adapter: NewsRVAdapter

    private var pageSize: Int = 20
    private var page: Int = 1

    lateinit var layoutManager: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        presenter = MainPresenter(this)
        presenter.getNews("technology", pageSize, page)

        layoutManager = LinearLayoutManager(this)
        rv_news_list.layoutManager = layoutManager

        adapter = NewsRVAdapter(listNews){
            startActivity<DetailActivity>("news" to it)
        }
        rv_news_list.adapter = adapter

        rv_news_list.addOnScrollListener(object: RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {

                if(dy>0){
                    val visibleItemCount = layoutManager.childCount
                    val pastVisibleItem = layoutManager.findFirstCompletelyVisibleItemPosition()
                    val total = adapter.itemCount

                    if ((visibleItemCount + pastVisibleItem) >= total) {
                        page++
                        presenter.getNews("technology", pageSize, page)
                    }
                }
                super.onScrolled(recyclerView, dx, dy)
            }

        })

    }

    override fun showNews(data: List<NewsData>) {
        listNews.addAll(data)
        adapter.notifyDataSetChanged()
    }

    override fun onError(throwable: Throwable) {
        toast(throwable.toString())

    }

    override fun showLoading() {
        pb_loading.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        pb_loading.visibility = View.GONE
    }
}
