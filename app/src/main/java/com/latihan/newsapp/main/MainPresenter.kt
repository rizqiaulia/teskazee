package com.latihan.newsapp.main

import com.latihan.newsapp.api.ApiInterface
import com.latihan.newsapp.model.NewsData
import com.latihan.newsapp.model.NewsResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainPresenter(private val view: MainView) {

    fun getNews(category: String, pageSize: Int, page: Int) {
        view.showLoading()

        ApiInterface.create()
            .getNewsTech(
                category,
                pageSize,
                page,
                "eeb96886dc514518919deb4e2310fd31"
            ).enqueue(object : Callback<NewsResponse> {
                override fun onFailure(call: Call<NewsResponse>, t: Throwable) {
                    view.onError(t)
                }

                override fun onResponse(
                    call: Call<NewsResponse>,
                    response: Response<NewsResponse>
                ) {
                    if (response.body()?.status.equals("ok") ){

                        response.body()?.articles?.let { view.showNews(it) }
                        view.hideLoading()
                    }else{
                        view.hideLoading()
                    }
                }

            })
    }
}