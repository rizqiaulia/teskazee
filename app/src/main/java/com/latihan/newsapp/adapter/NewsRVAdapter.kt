package com.latihan.newsapp.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.latihan.newsapp.R
import com.latihan.newsapp.model.NewsData
import kotlinx.android.synthetic.main.item_news.view.*

class NewsRVAdapter(private val listNews: MutableList<NewsData>,
                    private val listener: (NewsData) -> Unit) : RecyclerView.Adapter<NewsRVAdapter.NewsViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int)= NewsViewHolder (
        LayoutInflater.from(p0.context).inflate(R.layout.item_news, p0, false)
    )

    override fun getItemCount(): Int = listNews.size

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) = holder.bind(listNews[position], listener)

    class NewsViewHolder (view: View) : RecyclerView.ViewHolder(view){

        fun bind(list:NewsData,listener: (NewsData) -> Unit) = with(itemView){

            tv_title.text = list.title
            tv_author.text = list.author
            tv_desc.text = list.description

            setOnClickListener {
                listener(list)
            }

        }
    }

}