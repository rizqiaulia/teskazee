package com.latihan.newsapp.api

import com.latihan.newsapp.BuildConfig
import com.latihan.newsapp.model.NewsResponse
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {

    @GET("top-headlines")
    fun getNewsTech(@Query("category") category: String,
                    @Query("pageSize") pageSize: Int,
                    @Query("page") page: Int,
                    @Query("apiKey") apiKey: String): Call<NewsResponse>


    companion object {

        fun create(): ApiInterface {

            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BuildConfig.BASE_URL)
                .build()

            return retrofit.create(ApiInterface::class.java)

        }
    }
}