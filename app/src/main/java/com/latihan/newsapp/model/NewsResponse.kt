package com.latihan.newsapp.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NewsResponse(
    var status: String,
    var totalResults: Int,
    var articles: ArrayList<NewsData>
):Parcelable