package com.latihan.newsapp.detail

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.bumptech.glide.Glide
import com.latihan.newsapp.R
import com.latihan.newsapp.model.NewsData
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val intent = getIntent()
        val newsData  = intent.getParcelableExtra<NewsData>("news")

        tv_content.text = newsData.content
        tv_title_detail.text = newsData.title
        tv_author.text = newsData.author

        Glide.with(this).load(newsData.urlToImage).error(R.drawable.ic_launcher_background).into(iv_image)

    }
}
